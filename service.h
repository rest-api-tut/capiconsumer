#define PORT 8080
#define IP_ADDRES "127.0.0.1"

#define GET_USERS_REQUEST "GET /api/users HTTP/1.1\r\nHost: 127.0.0.1\n\r\n\r"
#define GET_USER_REQUEST "GET /api/users/1 HTTP/1.1\r\nHost: 127.0.0.1\n\r\n\r"
#define DELETE_USER_REQUEST "DELETE /api/users/1 HTTP/1.1\r\nHost: 127.0.0.1\n\r\n\r"
#define POST_USER_REQUEST "POST /api/users HTTP/1.1\r\nHost: 127.0.0.1\r\nContent-Type: application/json\r\nContent-Length: 200\n\r\n\r{\"id\": null, \"firstName\": \"Adam\", \"lastName\": \"XXX\", \"age\": 33}"

int set_up();
void clean_up();
void send_request(char* request);
void receive_and_print();

void show_users();
void show_user_with_id_1();
void delete_user_with_id_1();
void addUser();