#include "service.h"
#include "secrets.h"
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

int sockFd;

int set_up() {
    struct sockaddr_in sockaddr;

    printf("Linux Service ... Initialization\n");
    if((sockFd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Could not create socket");
        exit(1);
    }

    sockaddr.sin_family = AF_INET;
    sockaddr.sin_port = htons(PORT);
    sockaddr.sin_addr.s_addr = inet_addr(WSL_IP);
    
    if(connect(sockFd, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) == -1) {
        perror("Could not establish connection");
        return 1;
    }

    return 0;
}

void clean_up() {
    printf("Linux Service ... Cleaning up\n");
    shutdown(sockFd, SHUT_RDWR);
}

void send_request(char* request) {
    if(send(sockFd, request, strlen(request), 0) == -1) {
        perror("request failed");
    } else {
        receive_and_print();
    }
}

void receive_and_print() {
    char buf[2000];
    int responseSize = recv(sockFd, buf, sizeof(buf), 0);
    buf[responseSize] = '\0';

    printf("%s\n", buf);
    printf("Press any key to continue\n\n");
    getchar();
    getchar();
}

void show_users() {
    send_request(GET_USERS_REQUEST);
}

void show_user_with_id_1() {
    send_request(GET_USER_REQUEST);
}

void delete_user_with_id_1() {
    send_request(DELETE_USER_REQUEST);
}

void addUser() {
    send_request(POST_USER_REQUEST);
}