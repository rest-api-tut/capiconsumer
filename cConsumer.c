#include <stdio.h>
#include "service.h"

void showMenu();

int main(int argc, char const *argv[])
{
    char option;
    if(set_up() == 0) {
        do {
            showMenu();
            scanf("%s", &option);
            switch (option)
            {
            case '1': show_users(); break;
            case '2': show_user_with_id_1(); break;
            case '3': delete_user_with_id_1(); break;
            case '4': addUser(); break;
            }
        } while(option != '0');
    }

    clean_up();
    printf("\nEXIT\n");
    return 0;
}

void showMenu() {
    printf("1. Get users (GET)\n");
    printf("2. Get user with ID 1\n");
    printf("3. Delete user with ID 1\n");
    printf("4. Add user\n");
    printf("0. Exit\n");
    printf("Choose an option: ");
}