#include "service.h"
#include <stdio.h>
#include <WinSock2.h>

SOCKET ssoket;

int set_up() {
    WSADATA wsa;
    struct sockaddr_in sockaddr;

    printf("Windows Service ... Initialization\n");
    
    if(WSAStartup(MAKEWORD(2,2), &wsa) != 0) {
        perror("WSAStartup failed.\n");
        exit(EXIT_FAILURE);
    }

    if((ssoket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Could not create socket");
        exit(1);
    }

    sockaddr.sin_family = AF_INET;
    sockaddr.sin_port = htons(PORT);
    sockaddr.sin_addr.s_addr = inet_addr(IP_ADDRES);
    
    if(connect(ssoket, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) == -1) {
        perror("Could not establish connection");
        return 1;
    }

    return 0;
}

void clean_up() {
    printf("Windows Service ... Cleaning up\n");
    WSACleanup();
    shutdown(ssoket, SD_BOTH);
}

void send_request(char* request) {
    if(send(ssoket, request, strlen(request), 0) == -1) {
        perror("request failed");
    } else {
        receive_and_print();
    }
}

void receive_and_print() {
    char buf[2000];
    int responseSize = recv(ssoket, buf, sizeof(buf), 0);
    buf[responseSize] = '\0';

    printf("%s\n", buf);
    printf("Press any key to continue\n\n");
    getchar();
    getchar();
}

void show_users() {
    send_request(GET_USERS_REQUEST);
}

void show_user_with_id_1() {
    send_request(GET_USER_REQUEST);
}

void delete_user_with_id_1() {
    send_request(DELETE_USER_REQUEST);
}

void addUser() {
    send_request(POST_USER_REQUEST);
}
